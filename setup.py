import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
  name='weathergovapi',  
  version='0.2',
  author="Bryan Fordham",
  author_email="bryan@nativesavannah.com",
  description="Simple interface for weather.gov",
  long_description=long_description,
  long_description_content_type="text/markdown",
  url="https://gitlab.com/blfordham/weather-api",
  packages=setuptools.find_packages('./src'),
  package_dir={'weathergovapi': 'src/weathergovapi',},
  classifiers=[
      "Programming Language :: Python :: 3",
      "License :: OSI Approved :: MIT License",
      "Operating System :: OS Independent",
  ],
 )